function getIndexToIns(arr, num) {
  /*Return the lowest index at which a value (second argument) should be inserted into an array (first argument) once it has been sorted. The returned value should be a number.*/
  arr.sort(); //this is the way to sort in ascending order
  let arrLess = arr.filter(elem => elem < num);
  return arrLess.length;
}

getIndexToIns([40, 60], 50);
